Docker image for Emerging technology lecture.

Running:
`docker run -d -p 8888:8888 -e KERAS_BACKEND=tensorflow --name=emtech -v ~/notebook:/notebook fbcs/jupyter`

Create a bash file:
`touch mnist.sh`

Add to it:
```bash
#!/bin/bash

wget -P ~/notebook/data/ 'http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz' \
&& wget -P ~/notebook/data/ 'http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz' \
&& wget -P ~/notebook/data/ 'http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz' \
&& wget -P ~/notebook/data/ 'http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz' \
&& wget -P ~/notebook/ 'https://raw.githubusercontent.com/ianmcloughlin/jupyter-teaching-notebooks/master/mnist.ipynb'
```

Give user execute permision:
`sudo chmod u+x mnist.sh`

Run it to download files:
`./mnist.sh` - most likely you will need run it with `sudo`
